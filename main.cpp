#include <iostream>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Signals.h"

#include <Analyzer.h>

using namespace std;
using namespace llvm;


std::unique_ptr<llvm::Module> getModuleFromFile(llvm::LLVMContext &context, const StringRef file) {
    llvm::SMDiagnostic Err;

    auto Mod = llvm::parseIRFile(file, Err, context);

    if (Err.getLoc().isValid()) {
      std::cerr << Err.getMessage().str() << "\n";
      exit(1);
    }
    return Mod;
}


int main(int argc, char *argv[]) 
{
  llvm_shutdown_obj Y; // Call llvm_shutdown() on exit.
  LLVMContext context;

  llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);
  llvm::PrettyStackTraceProgram X(argc, argv);

  // Parse the command-line arguments
  if (argc <= 1) {
    std::cerr << argv[0] << ": error: no input files\n";
    return 1;
  }
	  unsigned countBranch = 0;
	  unsigned countCall =0;
	  unsigned countStore = 0;
	  unsigned countLoad = 0;
	  unsigned countArit =0;
	  unsigned countJump = 0;
	  unsigned countOther = 0;
  // For each input file specified in the command-line
  for (int narg = 1; narg < argc; ++narg) {
      const std::string file = argv[narg];

      // Parse the input file .ll and create a LLVM Module object.
      unique_ptr<Module> M = getModuleFromFile(context, file);
      if (!M) {
        std::cerr << "error: Invalid llvm assemble/bitcode file\n";
        return 1;
      }

      // Analyze the Key Performance Indicators (KPIs) of the program
      // in the LLVM intermediate representation.
      std::vector<int> contAux;
      contAux=analyzeKeyPerformanceIndicators(M.get());
      countBranch+=contAux[6];
      countCall+=contAux[5];
      countStore+=contAux[4];
      countLoad+=contAux[3];
      countArit+=contAux[2];
      countJump+=contAux[1];   
      countOther+=contAux[0];     
              
  if(narg==argc-1){   
	std::cout << "TOTAL BENCHMARK: ";     
	std::cout << "\n";   
	std::cout << "TOTAL BRANCH = " << countBranch << "\n";
	std::cout << "TOTAL CALL   = " << countCall << "\n";
	std::cout << "TOTAL STORE  = " << countStore << "\n";
	std::cout << "TOTAL LOAD   = " << countLoad << "\n";
	std::cout << "TOTAL ARIT   = " << countArit << "\n";
	std::cout << "TOTAL OTHER  = " << countOther << "\n";
	std::cout << "\n";
  }
      
  }


  return 0;
}

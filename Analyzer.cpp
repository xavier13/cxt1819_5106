#include "Analyzer.h"

#include "iostream"
#include "iomanip"
#include "vector"
#include "map"
#include "string"
#include "typeinfo"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"

using namespace llvm;
using namespace std;


std::vector<int> analyzeKeyPerformanceIndicators(const Module *M)
{
  // Collect statistics
  unsigned nFunctions = 0;
  unsigned nInstructions = 0;
  for (const Function &F : *M) {
      nFunctions++;
      for (const BasicBlock &BB : F) {
        for (const Instruction &I : BB) {
            nInstructions++;
        }
      }
  }

  // Get benchmark name
  string bench = M->getName().str();

  // Dump statistics
  std::cout << "   " << std::right << std::setw(5) << std::setfill(' ')<< nFunctions
            << "   " << std::right << std::setw(9) << std::setfill(' ')<< nInstructions   
            << "   " << bench		//Ruta
            << "\n";
            
  std::cout <<"\n";   
            
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
  std::cout << "3.1\n";    
  //Obtenemos vector de funciones 
  std::vector<string> sorted; 
  for (auto 	current = M->getFunctionList().begin(), end = M->getFunctionList().end(); current != end; ++current) {
					sorted.push_back(current->getName().str());
  }
	//Ordenamos la lista de func
	std::sort (sorted.begin(),sorted.end());
	
	//Iteramos la lista  ordenada			
	for(std::vector<string>::iterator iter=sorted.begin(); iter!=sorted.end(); ++iter){
		std::cout << ' ' << *iter << "\n";	
	}			
	
	std::cout << "\n";
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 
	
		std::cout << "3.2\n";
	//Tenemos un map de un string (nombre de la funcion llamante), y un vector (de funciones llamadas)
	std::map<std::string, std::vector<string>> mapa;

	auto &functionList = M->getFunctionList();
	//Vamos iterando para cada funcion de la lista
	for(auto &function : functionList){
		std::vector <string> vecfun;
		for(auto &bb : function) {	
			for(const auto &instruction : bb){
				//Comprobamos si la instruccion es del tipo callInst (llama a otra funcion)
				if(const CallInst *callInst = dyn_cast<CallInst>(&instruction)){
					Function *calledFunction = callInst->getCalledFunction();
					vecfun.push_back(calledFunction->getName().str());
				}
			}
		}
		//Ordenamos la lista de funciones de llamadas y la metemos en el map
		std::sort(vecfun.begin(), vecfun.end());
		//Eliminar duplciados
		vecfun.erase( unique( vecfun.begin(), vecfun.end() ), vecfun.end() );
		mapa.insert(std::pair<string,vector<string>>(function.getName().str(), vecfun));
	
	}

	//Sacamos por pantalla los resultados
	for(map< string, std::vector<string> >::iterator it=mapa.begin(); it!=mapa.end(); it++){
		std::cout << it->first <<  ": \n";
		vector<string>::iterator it1;
		auto vector = it->second;
		for(it1=vector.begin(); it1!=vector.end(); ++it1){
			std::cout << "   " << *it1 << "\n";	
		}
		
		std::cout << "\n";
	}			
	
	std::cout << "\n";
	
	
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
         
            

	std::cout << "3.3\n";
	
	std::string string1 ("omp_");
	std::string string2 ("__kmpc_");

	std::map<const Function *, std::vector< Function * > > mapa2;
	std::map<const Function *, std::vector< Function * > > mapaaux;

	auto &functionListring2 = M->getFunctionList();
	//Vamos iterando para cada funcion de la lista
	for(auto &function2 : functionListring2){
		std::vector <Function * > funs2;
		for(auto &bb2 : function2) {			
			for(const auto &instruction2 : bb2){	
			 //Comprobamos si la instruccion es del tipo callInst (llama a otra funcion)			
				if(const CallInst *callInstring2 = dyn_cast<CallInst>(&instruction2)){
					Function *calledFunction2 = callInstring2->getCalledFunction();
					auto string2 = calledFunction2->getName().str();	
					//Comparamos los nombres de las funciones con los strings que nos indican si son rutinas o directavas OpenMP				
					if((string1.compare(string2.substr(0,4))==0) or (string2.compare(string2.substr(0,7))==0)){
						funs2.push_back(calledFunction2);
					}
				}
			}			
		}
		
		//Se guardan las llamadas asociadas a su padre
		if(!(funs2.empty())){
			//Metemos los nombres de las funciones en un vector
			std::vector<string> functionsNames;
			for(std::vector< Function * >::iterator it=funs2.begin(); it!=funs2.end(); ++it){
				functionsNames.push_back((*it)->getName().str());	
			} 
			//Y lo ordenamos
			std::sort(functionsNames.begin(), functionsNames.end());
			
			//functionsNames.erase( unique( functionsNames.begin(), functionsNames.end() ), functionsNames.end() );
			
			//Ordenacion de los punteros apoyandose en la lista de nombres ya ordenada
			std::vector < Function * > funsort2;
			for(std::vector<string>::iterator it=functionsNames.begin(); it!=functionsNames.end(); ++it){
				string f2 = *it;
				for(std::vector< Function * >::iterator it2=funs2.begin(); it2!=funs2.end(); ++it2){
					if(f2.compare((*it2)->getName().str()) == 0) {
						funsort2.push_back((*it2));
					}
				}
			}
			
			//Mapa con las keys desordenadas pero sus vectores de llamadas ordenados
			mapaaux.insert(std::pair<const Function *,std::vector< Function * >>(&function2, funsort2));
		}
	}
	
	

	std::vector < string > functionsNamesAux2;
	for(map<const Function *, std::vector< Function * >>::const_iterator it=mapaaux.begin(); it!=mapaaux.end(); it++){
		functionsNamesAux2.push_back((it)->first->getName().str());	
	} 
	//Obtenermos las claves  ordenadas en este vector auxiliar
	std::sort(functionsNamesAux2.begin(), functionsNamesAux2.end());
	
	//Ordenacion para las keys
	for(std::vector< string >::iterator it=functionsNamesAux2.begin(); it!=functionsNamesAux2.end(); ++it){
		string f2 = *it;
		for(map<const Function *, std::vector< Function * >>::const_iterator it2=mapaaux.begin(); it2!=mapaaux.end(); it2++){
			if(f2.compare((it2)->first->getName().str()) == 0) {			
				//Se coge la funcion padre y el vector de calls asociado
				const Function * funP2 = it2->first;
				std::vector< Function * > vectorfuns2 = it2->second;
		
				//Se insertan los pares en el map definitivo, ordenado alfabeticamente
				mapa2.insert(std::pair<const Function *,std::vector< Function * >>(funP2, vectorfuns2));
			}
		}
	}
	
	
	//Se saca por pantalla
	for(map<const Function *, std::vector< Function * >>::const_iterator it=mapa2.begin(); it!=mapa2.end(); it++){
		std::cout << it->first->getName().str() <<  ": \n";
		vector< Function * >::iterator it2;
		auto vector2 = it->second;
		//Sacando los repetidos
		vector2.erase( unique( vector2.begin(), vector2.end() ), vector2.end() );
		for(it2=vector2.begin(); it2!=vector2.end(); ++it2){
			std::cout << "   " << (*it2)->getName().str() << "\n";	
		}
		
		std::cout << "\n";
			
	}			
	
	std::cout << "\n";
  
  unsigned countBranch = 0;
  unsigned countJump = 0;
  unsigned countArit = 0;
  unsigned countLoad = 0;
  unsigned countStore = 0;
  unsigned countCall = 0;  
  unsigned countOther = 0;          
  
   

	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 
std::cout << "LAB4\n";	 
std::cout << "\n";
std::cout << "SUBTOTALES\n";

//std::map<std::string, std::vector<string>> mapa3;


countBranch = 0;
countCall = 0;
countStore = 0;
countLoad = 0;
countArit = 0;
countJump = 0;
countOther = 0;
unsigned countBranchModule = 0;
unsigned countCallModule =0;
unsigned countStoreModule = 0;
unsigned countLoadModule = 0;
unsigned countAritModule =0;
unsigned countJumpModule = 0;
unsigned countOtherModule = 0;

for(auto &function: functionList){
  for(auto &bb: function){
    for(const auto &I: bb){
    	if(const BranchInst *branchInst = dyn_cast<BranchInst>(&I)){
        	countBranch++;;
        }
    	else if(const CallInst *callInst = dyn_cast<CallInst>(&I)){
	        countCall++;;
        }
        if(const BinaryOperator *aritInst = dyn_cast<BinaryOperator>(&I)){
	        countArit++;;
        }
        else if(const StoreInst *storeInst = dyn_cast<StoreInst>(&I)){
            countStore++;;
        }
        else if(const LoadInst *loadInst = dyn_cast<LoadInst>(&I)){
            countLoad++;;
        }
        else {
            countOther++;
        }
    }
  
  }
  std::cout << *sorted.begin() << "\n";
  sorted.erase(sorted.begin());
  //std::cout << "TOTAL FUNCTION:  " << *function->getName().str();
  std::cout << "TOTAL BRANCH = " << countBranch << "\n";
  std::cout << "TOTAL CALL   = " << countCall << "\n";
  std::cout << "TOTAL STORE  = " << countStore << "\n";
  std::cout << "TOTAL LOAD   = " << countLoad << "\n";
  std::cout << "TOTAL ARIT   = " << countArit << "\n";
  std::cout << "TOTAL OTHER  = " << countOther << "\n";
  std::cout << "\n";
  countBranchModule = countBranchModule+countBranch;
  countCallModule =countCallModule +countCall;
  countStoreModule = countStoreModule+countStore;
  countLoadModule = countLoadModule+countLoad;
  countAritModule =countAritModule +countArit;
  countJumpModule = countJumpModule+countJump;
  countOtherModule = countOtherModule+countOther;
  countBranch = 0;
  countCall = 0;
  countStore = 0;
  countLoad = 0;
  countArit = 0;
  countJump = 0;
  countOther = 0;
  
}
  std::cout << "TOTAL MODULE: ";
  std::cout << "\n";
  std::cout << "TOTAL BRANCH = " << countBranchModule << "\n";
  std::cout << "TOTAL CALL   = " << countCallModule << "\n";
  std::cout << "TOTAL STORE  = " << countStoreModule << "\n";
  std::cout << "TOTAL LOAD   = " << countLoadModule << "\n";
  std::cout << "TOTAL ARIT   = " << countAritModule << "\n";
  std::cout << "TOTAL OTHER  = " << countOtherModule << "\n";
  std::cout << "\n";

std::vector<int> datos; 
datos.push_back(countOtherModule);
datos.push_back(countJumpModule);
datos.push_back(countAritModule);
datos.push_back(countLoadModule);
datos.push_back(countStoreModule);
datos.push_back(countCallModule);
datos.push_back(countBranchModule);

return datos;

}	


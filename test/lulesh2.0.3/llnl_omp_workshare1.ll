; ModuleID = 'cxt1819_5106/test/llnl_omp_workshare1.c'
source_filename = "cxt1819_5106/test/llnl_omp_workshare1.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%ident_t = type { i32, i32, i32, i32, i8* }

@.str = private unnamed_addr constant [24 x i8] c"Number of threads = %d\0A\00", align 1
@.str.1 = private unnamed_addr constant [23 x i8] c"Thread %d starting...\0A\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c";unknown;unknown;0;0;;\00", align 1
@0 = private unnamed_addr constant %ident_t { i32 0, i32 2, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) }, align 8
@.str.3 = private unnamed_addr constant [22 x i8] c"Thread %d: c[%d]= %f\0A\00", align 1
@1 = private unnamed_addr constant %ident_t { i32 0, i32 66, i32 0, i32 0, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) }, align 8

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca [100 x float], align 16
  %11 = alloca [100 x float], align 16
  %12 = alloca [100 x float], align 16
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i32 0, i32* %8, align 4
  br label %13

; <label>:13:                                     ; preds = %27, %2
  %14 = load i32, i32* %8, align 4
  %15 = icmp slt i32 %14, 100
  br i1 %15, label %16, label %30

; <label>:16:                                     ; preds = %13
  %17 = load i32, i32* %8, align 4
  %18 = sitofp i32 %17 to double
  %19 = fmul double %18, 1.000000e+00
  %20 = fptrunc double %19 to float
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = getelementptr inbounds [100 x float], [100 x float]* %11, i64 0, i64 %22
  store float %20, float* %23, align 4
  %24 = load i32, i32* %8, align 4
  %25 = sext i32 %24 to i64
  %26 = getelementptr inbounds [100 x float], [100 x float]* %10, i64 0, i64 %25
  store float %20, float* %26, align 4
  br label %27

; <label>:27:                                     ; preds = %16
  %28 = load i32, i32* %8, align 4
  %29 = add nsw i32 %28, 1
  store i32 %29, i32* %8, align 4
  br label %13

; <label>:30:                                     ; preds = %13
  store i32 10, i32* %9, align 4
  call void (%ident_t*, i32, void (i32*, i32*, ...)*, ...) @__kmpc_fork_call(%ident_t* @0, i32 5, void (i32*, i32*, ...)* bitcast (void (i32*, i32*, i32*, i32*, [100 x float]*, [100 x float]*, [100 x float]*)* @.omp_outlined. to void (i32*, i32*, ...)*), i32* %6, i32* %9, [100 x float]* %12, [100 x float]* %10, [100 x float]* %11)
  %31 = load i32, i32* %3, align 4
  ret i32 %31
}

; Function Attrs: nounwind uwtable
define internal void @.omp_outlined.(i32* noalias, i32* noalias, i32* dereferenceable(4), i32* dereferenceable(4), [100 x float]* dereferenceable(400), [100 x float]* dereferenceable(400), [100 x float]* dereferenceable(400)) #0 {
  %8 = alloca i32*, align 8
  %9 = alloca i32*, align 8
  %10 = alloca i32*, align 8
  %11 = alloca i32*, align 8
  %12 = alloca [100 x float]*, align 8
  %13 = alloca [100 x float]*, align 8
  %14 = alloca [100 x float]*, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  store i32* %0, i32** %8, align 8
  store i32* %1, i32** %9, align 8
  store i32* %2, i32** %10, align 8
  store i32* %3, i32** %11, align 8
  store [100 x float]* %4, [100 x float]** %12, align 8
  store [100 x float]* %5, [100 x float]** %13, align 8
  store [100 x float]* %6, [100 x float]** %14, align 8
  %23 = load i32*, i32** %10, align 8
  %24 = load i32*, i32** %11, align 8
  %25 = load [100 x float]*, [100 x float]** %12, align 8
  %26 = load [100 x float]*, [100 x float]** %13, align 8
  %27 = load [100 x float]*, [100 x float]** %14, align 8
  %28 = call i32 @omp_get_thread_num() #3
  store i32 %28, i32* %16, align 4
  %29 = load i32, i32* %16, align 4
  %30 = icmp eq i32 %29, 0
  br i1 %30, label %31, label %35

; <label>:31:                                     ; preds = %7
  %32 = call i32 @omp_get_num_threads() #3
  store i32 %32, i32* %23, align 4
  %33 = load i32, i32* %23, align 4
  %34 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0), i32 %33)
  br label %35

; <label>:35:                                     ; preds = %31, %7
  %36 = load i32, i32* %16, align 4
  %37 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.1, i32 0, i32 0), i32 %36)
  store i32 0, i32* %18, align 4
  store i32 99, i32* %19, align 4
  store i32 1, i32* %20, align 4
  store i32 0, i32* %21, align 4
  %38 = load i32, i32* %24, align 4
  %39 = load i32*, i32** %8, align 8
  %40 = load i32, i32* %39, align 4
  call void @__kmpc_dispatch_init_4(%ident_t* @0, i32 %40, i32 35, i32 0, i32 99, i32 1, i32 %38)
  br label %41

; <label>:41:                                     ; preds = %81, %35
  %42 = load i32*, i32** %8, align 8
  %43 = load i32, i32* %42, align 4
  %44 = call i32 @__kmpc_dispatch_next_4(%ident_t* @0, i32 %43, i32* %21, i32* %18, i32* %19, i32* %20)
  %45 = icmp ne i32 %44, 0
  br i1 %45, label %46, label %82

; <label>:46:                                     ; preds = %41
  %47 = load i32, i32* %18, align 4
  store i32 %47, i32* %17, align 4
  br label %48

; <label>:48:                                     ; preds = %77, %46
  %49 = load i32, i32* %17, align 4, !llvm.mem.parallel_loop_access !1
  %50 = load i32, i32* %19, align 4, !llvm.mem.parallel_loop_access !1
  %51 = icmp sle i32 %49, %50
  br i1 %51, label %52, label %80

; <label>:52:                                     ; preds = %48
  %53 = load i32, i32* %17, align 4, !llvm.mem.parallel_loop_access !1
  %54 = mul nsw i32 %53, 1
  %55 = add nsw i32 0, %54
  store i32 %55, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %56 = load i32, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds [100 x float], [100 x float]* %26, i64 0, i64 %57
  %59 = load float, float* %58, align 4, !llvm.mem.parallel_loop_access !1
  %60 = load i32, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %61 = sext i32 %60 to i64
  %62 = getelementptr inbounds [100 x float], [100 x float]* %27, i64 0, i64 %61
  %63 = load float, float* %62, align 4, !llvm.mem.parallel_loop_access !1
  %64 = fadd float %59, %63
  %65 = load i32, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %66 = sext i32 %65 to i64
  %67 = getelementptr inbounds [100 x float], [100 x float]* %25, i64 0, i64 %66
  store float %64, float* %67, align 4, !llvm.mem.parallel_loop_access !1
  %68 = load i32, i32* %16, align 4, !llvm.mem.parallel_loop_access !1
  %69 = load i32, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %70 = load i32, i32* %22, align 4, !llvm.mem.parallel_loop_access !1
  %71 = sext i32 %70 to i64
  %72 = getelementptr inbounds [100 x float], [100 x float]* %25, i64 0, i64 %71
  %73 = load float, float* %72, align 4, !llvm.mem.parallel_loop_access !1
  %74 = fpext float %73 to double
  %75 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0), i32 %68, i32 %69, double %74), !llvm.mem.parallel_loop_access !1
  br label %76

; <label>:76:                                     ; preds = %52
  br label %77

; <label>:77:                                     ; preds = %76
  %78 = load i32, i32* %17, align 4, !llvm.mem.parallel_loop_access !1
  %79 = add nsw i32 %78, 1
  store i32 %79, i32* %17, align 4, !llvm.mem.parallel_loop_access !1
  br label %48, !llvm.loop !1

; <label>:80:                                     ; preds = %48
  br label %81

; <label>:81:                                     ; preds = %80
  br label %41

; <label>:82:                                     ; preds = %41
  %83 = load i32*, i32** %8, align 8
  %84 = load i32, i32* %83, align 4
  call void @__kmpc_barrier(%ident_t* @1, i32 %84)
  ret void
}

; Function Attrs: nounwind
declare i32 @omp_get_thread_num() #1

; Function Attrs: nounwind
declare i32 @omp_get_num_threads() #1

declare i32 @printf(i8*, ...) #2

declare void @__kmpc_dispatch_init_4(%ident_t*, i32, i32, i32, i32, i32, i32)

declare i32 @__kmpc_dispatch_next_4(%ident_t*, i32, i32*, i32*, i32*, i32*)

declare void @__kmpc_barrier(%ident_t*, i32)

declare void @__kmpc_fork_call(%ident_t*, i32, void (i32*, i32*, ...)*, ...)

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-19ubuntu1 (tags/RELEASE_391/rc2)"}
!1 = distinct !{!1}
